﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace webapi.Controllers
{
    public class DefaultController : ApiController
    {
        [Route("api/mappa")]
        public IHttpActionResult GetHelloWorld()
        {
            return Ok( new { message = "Hello World!!" });
        }
    }
}
