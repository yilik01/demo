// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var connect = require('gulp-connect');
var minifyCSS = require('gulp-minify-css');

// Folder Configuration
var root = 'app/';

var srcJS = [root + 'src/**/*.js'];
var srcSCSS = [root + 'src/**/*.scss'];
var srcHTML = [root + '*.html', root + 'src/**/*.html'];

var destJS = root + 'dist/js';
var destCSS = root + 'dist/css';

// Compile Our Sass
gulp.task('sass', function() {
    return gulp.src(srcSCSS)
        .pipe(sass())
        .pipe(gulp.dest(destCSS))
        .pipe(concat('app.debug.css'))
        .pipe(gulp.dest(destCSS))
        .pipe(minifyCSS())
        .pipe(rename('app.min.css'))
        .pipe(gulp.dest(destCSS))
        .pipe(connect.reload());
});

// Lint Task
gulp.task('lint', function() {
    return gulp.src(srcJS)
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// HTML 
gulp.task('html', function() {
    return gulp.src(srcHTML)
        .pipe(connect.reload());
});


// Concatenate & Minify JS
gulp.task('scripts', function() {
    return gulp.src(srcJS)
        .pipe(concat('app.debug.js'))
        .pipe(gulp.dest(destJS))
        .pipe(rename('app.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(destJS))
        .pipe(connect.reload());
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch(srcJS, ['lint', 'scripts']);
    gulp.watch(srcSCSS, ['sass']);
    gulp.watch(srcHTML, ['html']);
});

// Webserver
gulp.task('connect', function() {
    connect.server({
        root: root,
        port: 3000,
        livereload: true
    });
});

// Default Task
gulp.task('default', ['lint', 'sass', 'scripts', 'html', 'connect', 'watch']);