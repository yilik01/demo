===============================================================================
  Setting up the ALL IN ONE development server: VM-CEDDGIS
===============================================================================




===============================================================================
  Setting up the development PC:
===============================================================================

  To build and run the application:

  1. Install Node.js from http://nodejs.org/
  2. Install GIT from http://nodejs.org/
  3. In command prompt (i.e cmd):
     a. Install Bower by executing the following [npm install -g bower]
     b  cd to the directory
     c. Execute [npm install]
  4. Execute [npm install -g gulp]

  Running the above commands will download all the require libraries and
  dependency, build the solution, and open the web site in browser


  To add a new javascript via bower
  1. In a web broswer, open http://bower.io/search
  2. Search for the package
  3. If you find the package, you are after, copy the name of the package (e.g. package123)
  4. Open command prompt, and cd into you project directory (e.g. C:\Development\Project123).  
     This directory should already have a bower.json file.
  5. Execute [bower -install package123 -S]
  6. A new entry will be added to bower.json.
  7. Run [gulp]
  8. This will download the lib again to the project lib folder.
