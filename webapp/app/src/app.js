var app = angular.module("myApp", []);

app.controller("personController", function ($scope) {
    $scope.person = {
        firstName: "Jane",
        lastName: "Doe",
        fullName: function () {
            var x;
            x = $scope.person;
            return x.firstName + " " + x.lastName;
        }
    };

    $scope.fullName = function () {
        var x;
        x = $scope.person;
        return x.firstName + " " + x.lastName;
    };
});


app.controller("namesController", function ($scope) {
    $scope.names = [{
        name: 'Jani',
        country: 'Norway'
    }, {
        name: 'Thomase',
        country: 'Sweden'
    }, {
        name: 'Hege',
        country: 'Sweden'
    }, {
        name: 'Kai',
        country: 'Denmark'
    }];
});


app.controller("eventsController", function ($scope, $http) {

    $scope.count = 0;

    $scope.hideIcon = true;

    $scope.toggle = function () {
        $scope.hideIcon = !$scope.hideIcon;
    };

    $scope.executeGet = function () {

        $http.get("http://localhost:62896/api/mappa").then(function (response) {
            $scope.text = response.data.message;
        });
    };
});